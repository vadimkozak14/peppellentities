package shared_models_package

import "gitlab.com/vadimkozak14/peppellentities/entity"

type SupplyProduct struct {
	ID               uint    `json:"id"`
	SupplyID         uint    `json:"supply_id"`
	ProductID        *uint   `json:"product_id"`
	IsProductDeleted int   `json:"is_product_deleted"`
	Product          Product `json:"product"`
	Amount           int     `json:"amount"`
	Price            float64 `json:"price"`
}

func GetSupplyProduct(supplyProduct entity.SupplyProduct) SupplyProduct {
	supplyModelProduct := SupplyProduct{}
	supplyModelProduct.ID = supplyProduct.ID
	supplyModelProduct.Product = GetProduct(supplyProduct.Product)
	supplyModelProduct.Amount = supplyProduct.Amount
	supplyModelProduct.IsProductDeleted = supplyProduct.IsProductDeleted
	supplyModelProduct.ProductID = supplyProduct.ProductID
	supplyModelProduct.Price = supplyProduct.Price
	supplyModelProduct.SupplyID = supplyProduct.SupplyID
	return supplyModelProduct
}

func GetSupplyProductArray(array []entity.SupplyProduct) []SupplyProduct {
	formatted := []SupplyProduct{}
	for _, arrayEl := range array {
		formatted = append(formatted, GetSupplyProduct(arrayEl))
	}

	return formatted
}
