package shared_models_package

import "gitlab.com/vadimkozak14/peppellentities/entity"

type Point struct {
	ID           uint         `json:"id"`
	AdminID      uint         `json:"admin_id"`
	UserID       *uint        `json:"user_id"`
	Longitude    float64      `json:"longitude"`
	Latitude     float64      `json:"latitude"`
	Images       []PointImage `json:"images"`
	Comment      string       `json:"comment"`
	CityID       uint         `json:"city_id"`
	Type         string       `json:"type"`
	PhoneNumber  string       `json:"phone_number"`
	Address      string       `json:"address"`
	BusinessName string       `json:"business_name"`
	RouteName    string       `json:"route_name"`
	RouteID      uint         `json:"route_id"`
	Balance      int          `json:"balance"`
}

func GetPoint(point entity.Point) Point {
	pointModel := Point{}
	pointModel.ID = point.ID
	pointModel.UserID = point.UserID
	pointModel.AdminID = point.AdminID
	pointModel.Longitude = point.Longitude
	pointModel.Latitude = point.Latitude
	pointModel.Comment = point.Comment
	pointModel.CityID = point.CityID
	pointModel.Type = point.Type
	pointModel.PhoneNumber = point.PhoneNumber
	pointModel.Address = point.Address
	pointModel.BusinessName = point.BusinessName
	pointModel.Balance = point.Balance

	pointModel.Images = make([]PointImage, 0)
	for _, image := range point.PointImages {
		pointModel.Images = append(pointModel.Images, GetPointImage(image))
	}

	return pointModel
}

func GetPointArray(array []entity.Point) []Point {
	formatted := []Point{}
	for _, arrayEl := range array {
		formatted = append(formatted, GetPoint(arrayEl))
	}

	return formatted
}
