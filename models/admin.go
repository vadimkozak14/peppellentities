package shared_models_package

import "gitlab.com/vadimkozak14/peppellentities/entity"

type Admin struct {
	ID       uint   `json:"id"`
	Username string `json:"username"`
	Password string `json:"password"`
	Role     string `json:"role"`
}

func GetAdmin(admin entity.AdminUser) Admin {
	adminModel := Admin{}
	adminModel.ID = admin.ID
	adminModel.Username = admin.Username
	adminModel.Password = admin.Password
	adminModel.Role = admin.Role

	return adminModel
}

func GetAdminArray(array []entity.AdminUser) []Admin {
	formatted := []Admin{}
	for _, arrayEl := range array {
		formatted = append(formatted, GetAdmin(arrayEl))
	}

	return formatted
}
