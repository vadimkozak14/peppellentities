package shared_models_package

import (
	"time"

	"gitlab.com/vadimkozak14/peppellentities/entity"
)

type AgentStatusLog struct {
	ID        uint      `json:"id"`
	UserID    uint      `json:"user_id"`
	OnlineAt  time.Time `json:"online_at"`
	OfflineAt time.Time `json:"offline_at"`
	Track     string    `json:"track"`
	RouteID   uint      `json:"route_id"`
	RouteName string    `json:"route_name"`
}

func GetAgentStatusLog(agentStatusLog entity.AgentStatusLog) AgentStatusLog {
	agentStatusLogModel := AgentStatusLog{}
	agentStatusLogModel.ID = agentStatusLog.ID
	agentStatusLogModel.UserID = agentStatusLog.UserID
	agentStatusLogModel.OnlineAt = agentStatusLog.OnlineAt
	agentStatusLogModel.OfflineAt = agentStatusLog.OfflineAt
	agentStatusLogModel.Track = agentStatusLog.Track
	agentStatusLogModel.RouteID = agentStatusLog.RouteID
	agentStatusLogModel.RouteName = agentStatusLog.RouteName
	return agentStatusLogModel
}

func GetAgentStatusLogArray(array []entity.AgentStatusLog) []AgentStatusLog {
	formatted := []AgentStatusLog{}
	for _, arrayEl := range array {
		formatted = append(formatted, GetAgentStatusLog(arrayEl))
	}

	return formatted
}
