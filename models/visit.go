package shared_models_package

import (
	"gitlab.com/vadimkozak14/peppellentities/entity"
    "time"
)
type Visit struct {
	ID             uint       `json:"id"`
	PointID        *uint      `json:"point_id"`
	Point          Point      `json:"point"`
	UserID         uint       `json:"user_id"`
	User           User       `json:"user"`
	IsNFC          bool       `json:"is_nfc"`
	StartLongitude float64    `json:"start_longitude"`
	StartLatitude  float64    `json:"start_latitude"`
	EndLongitude   float64    `json:"end_longitude"`
	EndLatitude    float64    `json:"end_latitude"`
	HasSupplies    bool       `json:"has_supplies"`
	StartTime      *time.Time `json:"start_time"`
	EndTime        *time.Time `json:"end_time"`
}

func GetVisit(visit entity.Visit) Visit {
	VisitModel := Visit{}
	VisitModel.ID = visit.ID
	VisitModel.User = GetUser(visit.User)
	VisitModel.UserID = visit.UserID
	VisitModel.Point = GetPoint(visit.Point)
	VisitModel.PointID = visit.PointID
	VisitModel.IsNFC = intToBool(visit.IsNFC)
	VisitModel.StartLongitude = visit.StartLongitude
	VisitModel.StartLatitude = visit.StartLatitude
	VisitModel.EndLongitude = visit.EndLongitude
	VisitModel.EndLatitude = visit.EndLatitude
	VisitModel.StartTime = visit.StartTime
	VisitModel.EndTime = visit.EndTime

	return VisitModel
}

func GetVisitArray(array []entity.Visit) []Visit {
	formatted := []Visit{}
	for _, arrayEl := range array {
		formatted = append(formatted, GetVisit(arrayEl))
	}

	return formatted
}


func intToBool(i int) bool {
	if i == 1 {
		return true
	}
	return false
}

