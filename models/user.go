package shared_models_package

import "gitlab.com/vadimkozak14/peppellentities/entity"

type User struct {
	ID           uint       `json:"id"`
	Email        *string    `json:"email"`
	Name         string     `json:"name"`
	Surname      string     `json:"surname"`
	PhoneNumber  string     `json:"phone_number"`
	ProfileImage string     `json:"profile_image"`
	AdminID      uint       `json:"admin_id"`
	CityID       *uint      `json:"city_id"`
	Documents    []Document `json:"documents"`
	JobStatus    string     `json:"job_status"`
	Status       string     `json:"status"`
}

func GetUser(user entity.User) User {
	userModel := User{}
	userModel.ID = user.ID
	userModel.Email = user.Email
	userModel.PhoneNumber = user.PhoneNumber
	userModel.ProfileImage = user.ProfileImage
	userModel.Surname = user.Surname
	userModel.Name = user.Name
	userModel.CityID = user.CityID
	userModel.JobStatus = user.JobStatus
	userModel.Status = user.Status
	userModel.AdminID = user.AdminID

	userModel.Documents = make([]Document, 0)

	for _, document := range user.Documents {
		userModel.Documents = append(userModel.Documents, GetDocument(document))
	}

	return userModel
}

func GetUserArray(array []entity.User) []User {
	formatted := []User{}
	for _, arrayEl := range array {
		formatted = append(formatted, GetUser(arrayEl))
	}

	return formatted
}
