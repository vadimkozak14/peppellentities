package shared_models_package

import "gitlab.com/vadimkozak14/peppellentities/entity"

type Document struct {
	ID     uint   `json:"id"`
	UserID uint   `json:"user_id"`
	Path   string `json:"path"`
}

func GetDocument(document entity.Document) Document {
	documentModel := Document{}
	documentModel.ID = document.ID
	documentModel.UserID = document.UserID
	documentModel.Path = document.Path

	return documentModel
}

func GetDocumentArray(array []entity.Document) []Document {
	formatted := []Document{}
	for _, arrayEl := range array {
		formatted = append(formatted, GetDocument(arrayEl))
	}

	return formatted
}
