package shared_models_package

import "gitlab.com/vadimkozak14/peppellentities/entity"

type Product struct {
	ID            uint           `json:"id"`
	Name          string         `json:"name"`
	Description   string         `json:"description"`
	Price         float64        `json:"price"`
	ProductImages []ProductImage `json:"product_images"`
	Barcode       string         `json:"barcode"`
	RubricID      *uint          `json:"rubric_id"`
	Rubric        Rubric         `json:"rubric"`
}

func GetProduct(product entity.Product) Product {
	productModel := Product{}
	productModel.ID = product.ID
	productModel.Name = product.Name
	productModel.Description = product.Description
	productModel.Price = product.Price
	productModel.ProductImages = make([]ProductImage, 0)
	for _, productImg := range product.ProductImages {
		productModel.ProductImages = append(productModel.ProductImages, GetProductImage(productImg))
	}
	productModel.Barcode = product.Barcode
	productModel.RubricID = product.RubricID
	productModel.Rubric = GetRubric(product.Rubric)
	return productModel
}

func GetProductArray(array []entity.Product) []Product {
	formatted := []Product{}
	for _, arrayEl := range array {
		formatted = append(formatted, GetProduct(arrayEl))
	}

	return formatted
}
