package shared_models_package

import (
	"gitlab.com/vadimkozak14/peppellentities/entity"
)

type ProductStatistic struct {
	ID        uint `json:"id"`
	SupplyID  uint `json:"supply_id"`
	UserID    *uint   `json:"user_id"`
	PointID   *uint   `json:"point_id"`
	ProductID uint    `json:"product_id"`
	CityID    uint    `json:"city_id"`
	Date      string  `json:"date"`
	Status    string  `json:"status"`
	Amount    int     `json:"amount"`
	Price     float64 `json:"price"`
	Product Product `json:"product"`
}

func GetProductStatistic(productStatistic entity.ProductStatistic) ProductStatistic {
	productStatisticModel := ProductStatistic{}
	productStatisticModel.ID = productStatistic.ID
	productStatisticModel.SupplyID = productStatistic.SupplyID
	productStatisticModel.ProductID = productStatistic.ProductID
	productStatisticModel.UserID = productStatistic.UserID
	productStatisticModel.PointID = productStatistic.PointID
	productStatisticModel.CityID = productStatistic.CityID
	productStatisticModel.Date = productStatistic.Date
	productStatisticModel.Status = productStatistic.Status
	productStatisticModel.Amount = productStatistic.Amount
	productStatisticModel.Price = productStatistic.Price
	productStatisticModel.Product = GetProduct(productStatistic.Product)

	return productStatisticModel
}

func GetProductStatistics(productStatistics []entity.ProductStatistic) []ProductStatistic {
	var productStatisticModels []ProductStatistic

	for _, productStatistic := range productStatistics {
		productStatisticModels = append(productStatisticModels, GetProductStatistic(productStatistic))
	}

	return productStatisticModels
}
