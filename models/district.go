package shared_models_package

import (
	"gitlab.com/vadimkozak14/peppellentities/entity"

	"gorm.io/datatypes"
)

type District struct {
	ID          uint   `json:"id"`
	Name        string `json:"name"`
	Okato       string `json:"okato"`
	Abbrev      string `json:"abbrev"`
	Type        string `json:"type"`
	Coordinates datatypes.JSON
}

func GetDistrict(district entity.District) District {
	districtModel := District{}
	districtModel.ID = district.ID
	districtModel.Name = district.Name
	districtModel.Okato = district.Okato
	districtModel.Abbrev = district.Abbrev
	districtModel.Type = district.Type
	districtModel.Coordinates = district.Coordinates

	return districtModel
}

func GetDistrictArray(array []entity.District) []District {
	formatted := []District{}
	for _, arrayEl := range array {
		formatted = append(formatted, GetDistrict(arrayEl))
	}

	return formatted
}
