package shared_models_package

import (
	"gitlab.com/vadimkozak14/peppellentities/entity"
)

type Supply struct {
	ID             uint            `json:"id"`
	PointID        *uint           `json:"point_id"`
	Point          Point           `json:"point"`
	UserID         *uint           `json:"user_id"`
	User           User            `json:"user"`
	TotalPrice     float64         `json:"total_price"`
	TotalAmount    int             `json:"total_amount"`
	SupplyProducts []SupplyProduct `json:"supply_products"`
	Status         string          `json:"status"`
	CityID         *uint           `json:"city_id"`
	City           City            `json:"city"`
	VisitID        *uint           `json:"visit_id"`
	CourierID      *uint           `json:"courier_id"`
	Courier        User            `json:"courier"`
	
}

func GetSupply(supply entity.Supply) Supply {
	supplyModel := Supply{}
	supplyModel.ID = supply.ID
	supplyModel.SupplyProducts = make([]SupplyProduct, 0)
	for _, sp := range supply.SupplyProducts {
		supplyModel.SupplyProducts = append(supplyModel.SupplyProducts, GetSupplyProduct(sp))
	}
	supplyModel.PointID = supply.PointID
	supplyModel.Point = GetPoint(supply.Point)
	supplyModel.UserID = supply.UserID
	supplyModel.User = GetUser(supply.User)
	supplyModel.TotalPrice = supply.TotalPrice
	supplyModel.Status = supply.Status
	supplyModel.TotalAmount = supply.TotalAmount
	supplyModel.CityID = supply.CityID
	supplyModel.City = GetCity(supply.City)
	supplyModel.VisitID = supply.VisitID
	supplyModel.CourierID = supply.CourierID
	supplyModel.Courier = GetUser(supply.Courier)
	return supplyModel
}

func GetSupplyArray(array []entity.Supply) []Supply {
	formatted := []Supply{}
	for _, arrayEl := range array {
		formatted = append(formatted, GetSupply(arrayEl))
	}

	return formatted
}
