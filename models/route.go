package shared_models_package

import (
	"gitlab.com/vadimkozak14/peppellentities/entity"
	"gorm.io/datatypes"
)

type Route struct {
	ID           uint           `json:"id"`
	Name         string         `json:"name"`
	UserID       *uint          `json:"user_id"`
	Route        datatypes.JSON `json:"route"`
	UserName     string         `json:"user_name"`
	Description  string         `json:"description"`
	District     string         `json:"district"`
	Length       int            `json:"length"`
	PointsAmount int            `json:"points_amount"`
	Day          int            `json:"day"`
	CityID       *uint          `json:"city_id"`
	City         City           `json:"city"`
	UserSurname  string         `json:"user_surname"`
}

func GetRoute(route entity.Route) Route {
	routeModel := Route{}
	routeModel.ID = route.ID
	routeModel.Name = route.Name
	routeModel.UserID = route.UserID
	routeModel.Route = route.Route
	routeModel.Description = route.Description
	routeModel.District = route.District
	routeModel.Length = route.Length
	routeModel.PointsAmount = route.PointsAmount
	routeModel.UserName = route.User.Name
	routeModel.UserSurname = route.User.Surname
	dayInt := route.Day
	routeModel.Day = dayInt
	routeModel.City = GetCity(route.City)
	routeModel.CityID = route.CityID
	return routeModel
}

func GetRouteArray(array []entity.Route) []Route {
	formatted := []Route{}
	for _, arrayEl := range array {
		formatted = append(formatted, GetRoute(arrayEl))
	}

	return formatted
}
