package shared_models_package

import "gitlab.com/vadimkozak14/peppellentities/entity"

type ProductImage struct {
	ID        uint   `json:"id"`
	ProductID uint   `json:"product_id"`
	Path      string `json:"path"`
}

func GetProductImage(productImage entity.ProductImage) ProductImage {
	productImageModel := ProductImage{}
	productImageModel.ID = productImage.ID
	productImageModel.ProductID = productImage.ProductID
	productImageModel.Path = productImage.Path
	return productImageModel
}

func GetProductImageArray(array []entity.ProductImage) []ProductImage {
	formatted := []ProductImage{}
	for _, arrayEl := range array {
		formatted = append(formatted, GetProductImage(arrayEl))
	}

	return formatted
}
