package shared_models_package

import "gitlab.com/vadimkozak14/peppellentities/entity"

type PointImage struct {
	ID      uint   `json:"id"`
	PointID uint   `json:"point_id"`
	Path    string `json:"path"`
}

func GetPointImage(point entity.PointImage) PointImage {
	pointImage := PointImage{}
	pointImage.ID = point.ID
	pointImage.PointID = point.PointID
	pointImage.Path = point.Path

	return pointImage
}

func GetPointImageArray(array []entity.PointImage) []PointImage {
	formatted := []PointImage{}
	for _, arrayEl := range array {
		formatted = append(formatted, GetPointImage(arrayEl))
	}

	return formatted
}
