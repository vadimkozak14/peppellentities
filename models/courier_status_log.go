package shared_models_package

import (
	"time"

	"gitlab.com/vadimkozak14/peppellentities/entity"
)

type CourierStatusLog struct {
	ID        uint      `json:"id"`
	UserID    uint      `json:"user_id"`
	OnlineAt  time.Time `json:"online_at"`
	OfflineAt time.Time `json:"offline_at"`
	Track     string    `json:"track"`
}

func GetCourierStatusLog(courierStatusLog entity.CourierStatusLog) CourierStatusLog {
	courierStatusLogModel := CourierStatusLog{}
	courierStatusLogModel.ID = courierStatusLog.ID
	courierStatusLogModel.UserID = courierStatusLog.UserID
	courierStatusLogModel.OnlineAt = courierStatusLog.OnlineAt
	courierStatusLogModel.OfflineAt = courierStatusLog.OfflineAt
	courierStatusLogModel.Track = courierStatusLog.Track
	return courierStatusLogModel
}

func GetCourierStatusLogArray(array []entity.CourierStatusLog) []CourierStatusLog {
	formatted := []CourierStatusLog{}
	for _, arrayEl := range array {
		formatted = append(formatted, GetCourierStatusLog(arrayEl))
	}

	return formatted
}
