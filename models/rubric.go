package shared_models_package

import "gitlab.com/vadimkozak14/peppellentities/entity"

type Rubric struct {
	ID   uint   `json:"id"`
	Name string `json:"name"`
}

func GetRubric(rubric entity.Rubric) Rubric {
	rubricModel := Rubric{}
	rubricModel.ID = rubric.ID
	rubricModel.Name = rubric.Name
	return rubricModel
}

func GetRubricArray(array []entity.Rubric) []Rubric {
	formatted := []Rubric{}
	for _, arrayEl := range array {
		formatted = append(formatted, GetRubric(arrayEl))
	}

	return formatted
}
