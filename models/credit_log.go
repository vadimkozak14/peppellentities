package shared_models_package

import "gitlab.com/vadimkozak14/peppellentities/entity"

type CreditLog struct {
	ID            uint   `json:"id"`
	AddAdminID    uint   `json:"add_admin_id"`
	AddAdmin      Admin  `json:"add_admin"`
	CancelAdminID *uint   `json:"cancel_admin_id"`
	CancelAdmin   Admin  `json:"cancel_admin"`
	PointID       uint   `json:"point_id"`
	Point         Point  `json:"point"`
	IsCanceled    int    `json:"is_canceled"`
	Amount        int    `json:"amount"`
}

func GetCreditLog(creditLog entity.CreditLog) CreditLog {
	creditLogModel := CreditLog{}
	creditLogModel.ID = creditLog.ID
	creditLogModel.AddAdminID = creditLog.AddAdminID
	creditLogModel.AddAdmin = GetAdmin(creditLog.AddAdmin)
	creditLogModel.CancelAdminID = creditLog.CancelAdminID
	creditLogModel.CancelAdmin = GetAdmin(creditLog.CancelAdmin)
	creditLogModel.PointID = creditLog.PointID
	creditLogModel.Point = GetPoint(creditLog.Point)
	creditLogModel.IsCanceled = creditLog.IsCanceled
	creditLogModel.Amount = creditLog.Amount

	return creditLogModel
}

func GetCreditLogArray(array []entity.CreditLog) []CreditLog {
	formatted := []CreditLog{}
	for _, arrayEl := range array {
		formatted = append(formatted, GetCreditLog(arrayEl))
	}

	return formatted
}
