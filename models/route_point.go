package shared_models_package

import "gitlab.com/vadimkozak14/peppellentities/entity"

type RoutePoint struct {
	ID      uint  `json:"id"`
	PointID uint  `json:"point_id"`
	RouteID uint  `json:"route_id"`
	Order   int   `json:"order"`
	Point   Point `json:"point"`
}

func GetRoutePoint(routePoint entity.RoutePoint) RoutePoint {
	routePointModel := RoutePoint{}
	routePointModel.ID = routePoint.ID
	routePointModel.PointID = routePoint.PointID
	routePointModel.RouteID = routePoint.RouteID
	routePointModel.Order = routePoint.Order
	routePointModel.Point = GetPoint(routePoint.Point)

	return routePointModel
}

func GetRoutePointArray(array []entity.RoutePoint) []RoutePoint {
	formatted := []RoutePoint{}
	for _, arrayEl := range array {
		formatted = append(formatted, GetRoutePoint(arrayEl))
	}

	return formatted
}
