package shared_models_package

import "gitlab.com/vadimkozak14/peppellentities/entity"

type City struct {
	ID   uint   `json:"id"`
	Name string `json:"city_name"`
}

func GetCity(city entity.City) City {
	cityModel := City{}
	cityModel.ID = city.ID
	cityModel.Name = city.Name

	return cityModel
}

func GetCityArray(array []entity.City) []City {
	formatted := []City{}
	for _, arrayEl := range array {
		formatted = append(formatted, GetCity(arrayEl))
	}

	return formatted
}
