package shared_models_package

import "gitlab.com/vadimkozak14/peppellentities/entity"

type PointComment struct {
	ID          uint   `json:"id"`
	Text        string `json:"text"`
	UserID      uint   `json:"user_id"`
	PointID     uint   `json:"point_id"`
	Image       string `json:"image"`
	UserName    string `json:"user_name"`
	UserSurname string `json:"user_surname"`
}

func GetPointComment(pointComment entity.PointComment) PointComment {
	pointCommentModel := PointComment{}
	pointCommentModel.ID = pointComment.ID
	pointCommentModel.Text = pointComment.Text
	pointCommentModel.UserID = pointComment.UserID
	pointCommentModel.PointID = pointComment.PointID
	pointCommentModel.Image = pointComment.Image
	return pointCommentModel
}

func GetPointCommentArray(array []entity.PointComment) []PointComment {
	formatted := []PointComment{}
	for _, arrayEl := range array {
		formatted = append(formatted, GetPointComment(arrayEl))
	}

	return formatted
}
