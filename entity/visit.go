package entity

import (
	"gorm.io/gorm"
	"time"
)

type Visit struct {
	gorm.Model
	ID             uint
	IsNFC          int
	UserID         uint       `gorm:"type:bigint(20);index"`
	User           User       `gorm:"foreignkey:user_id;references:id"`
	PointID        *uint      `gorm:"type:bigint(20);index"`
	Point          Point      `gorm:"foreignkey:point_id;references:id"`
	StartLongitude float64    `gorm:"type:decimal(14, 9)"`
	StartLatitude  float64    `gorm:"type:decimal(14, 9)"`
	EndLongitude   float64    `gorm:"type:decimal(14, 9)"`
	EndLatitude    float64    `gorm:"type:decimal(14, 9)"`
	StartTime      *time.Time `gorm:"default:null"`
	EndTime        *time.Time `gorm:"default:null"`
}
