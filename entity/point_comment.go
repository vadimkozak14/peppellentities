package entity

import "gorm.io/gorm"

type PointComment struct {
	gorm.Model
	ID      uint
	UserID  uint   `gorm:"type:bigint(20)"`
	PointID uint   `gorm:"type:bigint(20)"`
	Image   string `gorm:"size:500;"`
	Text    string `gorm:"type:text"`
}
