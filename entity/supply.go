package entity

import (
	"gorm.io/gorm"
)

type Supply struct {
	gorm.Model
	ID             uint
	PointID        *uint   `gorm:"type:bigint(20);index"`
	Point          Point   `gorm:"foreignkey:point_id;references:id"`
	UserID         *uint   `gorm:"type:bigint(20);index"`
	User           User    `gorm:"foreignkey:user_id;references:id"`
	CityID         *uint   `gorm:"type:bigint(20);index"`
	City           City    `gorm:"foreignkey:city_id;references:id"`
	TotalPrice     float64 `gorm:"type:decimal(18, 9)"`
	SupplyProducts []SupplyProduct
	Status         string `gorm:"type:enum('new', 'packing', 'packed', 'delivering', 'delivered', 'not_delivered', 'paid', 'cancelled');default:'new'"`
	TotalAmount    int
	VisitID        *uint
	CourierID      *uint   `gorm:"type:bigint(20);index"`
	Courier        User    `gorm:"foreignkey:courier_id;references:id"`
}
