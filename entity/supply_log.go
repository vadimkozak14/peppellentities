package entity

import (
	"gorm.io/datatypes"
	"gorm.io/gorm"
)

type SupplyLog struct {
	gorm.Model
	ID        uint
	ChangerID uint
	Action    string `gorm:"type:enum('create', 'update', 'delete')"`
	Changer   string `gorm:"type:enum('user', 'admin')"`
	Data      datatypes.JSON
}
