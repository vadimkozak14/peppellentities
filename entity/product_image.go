package entity

import "gorm.io/gorm"

type ProductImage struct {
	gorm.Model
	ID        uint
	ProductID uint
	Path      string `gorm:"type:text"`
}
