package entity

import "gorm.io/gorm"

type Product struct {
	gorm.Model
	ID            uint
	Name          string  `gorm:"type:text"`
	Description   string  `gorm:"type:text"`
	Price         float64 `gorm:"type:decimal(18, 9)"`
	ProductImages []ProductImage
	Barcode       string `gorm:"type:text"`
	RubricID      *uint  `gorm:"type:bigint(20);index"`
	Rubric        Rubric `gorm:"foreignkey:rubric_id;references:id"`
}
