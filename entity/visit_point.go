package entity

import (
	"gorm.io/gorm"
)

type VisitPoint struct {
	gorm.Model
	ID      uint
	PointID uint
	RouteID uint
	UserID  uint
	Day     string
	Visited int
}
