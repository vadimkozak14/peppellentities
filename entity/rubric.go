package entity

import "gorm.io/gorm"

type Rubric struct {
	gorm.Model
	ID   uint
	Name string `gorm:"type:text"`
}
