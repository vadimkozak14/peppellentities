package entity

import "gorm.io/gorm"

type ProductStatistic struct {
	gorm.Model
	ID        uint
	SupplyID  uint
	UserID    *uint   `gorm:"type:bigint(20)"`
	PointID   *uint   `gorm:"type:bigint(20)"`
	ProductID uint    `gorm:"type:bigint(20)"`
	Product   Product `gorm:"foreignkey:product_id;references:id"`
	CityID    uint    `gorm:"type:bigint(20)"`
	Date      string
	Status    string `gorm:"type:enum('new', 'packing', 'packed', 'delivering', 'delivered', 'not_delivered', 'paid', 'cancelled')"`
	Amount    int
	Price     float64 `gorm:"type:decimal(18, 9)"`
}
