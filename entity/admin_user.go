package entity

import (
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

type AdminUser struct {
	gorm.Model
	ID       uint
	Username string `gorm:"type:varchar(255);"`
	Password string `gorm:"type:varchar(255);"`
	Role     string `gorm:"type:enum('super_admin', 'supervisor', 'supplier', 'cashier');default:'supervisor'"`
}

func (adminUser *AdminUser) SetPasswordHash() {
	bytes, _ := bcrypt.GenerateFromPassword([]byte(adminUser.Password), bcrypt.DefaultCost)
	adminUser.Password = string(bytes)
}
