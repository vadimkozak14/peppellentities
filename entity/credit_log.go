package entity

import "gorm.io/gorm"

type CreditLog struct {
	gorm.Model
	ID            uint
	AddAdminID    uint   `gorm:"type:bigint(20)"`
	AddAdmin      AdminUser  `gorm:"foreignkey:add_admin_id;references:id"`
	CancelAdminID *uint   `gorm:"type:bigint(20)"`
	CancelAdmin   AdminUser  `gorm:"foreignkey:cancel_admin_id;references:id"`
	PointID       uint   `gorm:"type:bigint(20)"`
	Point         Point  `gorm:"foreignkey:point_id;references:id"`
	IsCanceled    int
	Amount        int
}
