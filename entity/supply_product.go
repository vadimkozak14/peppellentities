package entity

import (
	"gorm.io/gorm"
)

type SupplyProduct struct {
	gorm.Model
	ID               uint
	SupplyID         uint
	ProductID        *uint   `gorm:"type:bigint(20);index"`
	IsProductDeleted int
	Product          Product `gorm:"foreignkey:product_id;references:id"`
	Amount           int
	Price            float64 `gorm:"type:decimal(18, 9)"`
}
