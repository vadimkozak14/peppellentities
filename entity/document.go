package entity

import "gorm.io/gorm"

type Document struct {
	gorm.Model
	ID     uint
	UserID uint `gorm:"type:bigint(20)"`
	Path   string
}
